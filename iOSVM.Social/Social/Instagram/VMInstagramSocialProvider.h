//
//  VMInstagramSocialProvider.h
//  WeDo
//
//  Created by Alice on 11/11/2016.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMSocialProvider.h"

@interface VMInstagramSocialProvider : NSObject<VMSocialProvider>

@end
