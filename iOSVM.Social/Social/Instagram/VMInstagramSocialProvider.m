//
//  VMInstagramSocialProvider.m
//  WeDo
//
//  Created by Alice on 11/11/2016.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

#import "VMInstagramSocialProvider.h"
#import <InstagramKit/InstagramKit.h>
#import "VMFlashManager.h"

@interface VMInstagramSocialProvider() <UIWebViewDelegate, UIDocumentInteractionControllerDelegate>
{
    UIWebView *webView;
    UIDocumentInteractionController *documentController;
    VMSocialAuthBlock callback;
}
@end

@implementation VMInstagramSocialProvider

- (NSString *)socialNetworkName;
{
    return @"Instagram";
}

- (void)authorizeWithBlock:(VMSocialAuthBlock)block;
{
    if([InstagramEngine sharedEngine].accessToken != nil) {
        if (block) {
            block(nil, [InstagramEngine sharedEngine].accessToken, nil);
        }
        return;
    }
    
    callback = block;
    NSURL *authURL = [[InstagramEngine sharedEngine] authorizationURL];
    webView = [UIWebView new];
    webView.delegate = self;
    webView.frame = [UIApplication sharedApplication].keyWindow.frame;
    [[UIApplication sharedApplication].keyWindow addSubview:webView];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:webView];
    [webView loadRequest:[NSURLRequest requestWithURL:authURL]];
}

- (void)share:(VMShareInfo *)info;
{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if(![[UIApplication sharedApplication] canOpenURL:instagramURL]){
        [[VMFlashManager sharedManager] showFlash:@"Instagram app is not installed"];
    }
    
    UIImage *imageToUse = info.images.firstObject;
    NSString *documentDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *saveImagePath = [documentDirectory stringByAppendingPathComponent:@"Image.igo"];
    NSData *imageData = UIImagePNGRepresentation(imageToUse);
    [imageData writeToFile:saveImagePath atomically:YES];
    NSURL *imageURL = [NSURL fileURLWithPath:saveImagePath];
    
    documentController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
    documentController.delegate = self;
    documentController.annotation = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"Testing"], @"InstagramCaption", nil];
    documentController.UTI = @"com.instagram.exclusivegram";
    [documentController presentOpenInMenuFromRect:CGRectMake(1, 1, 1, 1) inView: [UIApplication sharedApplication].keyWindow.rootViewController.view animated: YES];
}

- (void)shareWithUi:(VMShareInfo *)info withBlock:(VMCompletionBlock)block;
{
    [NSException raise:@"Not implemented yet" format:@"instagram share"];
}

- (BOOL)webView:(UIWebView *)_webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSError *error;
    if ([[InstagramEngine sharedEngine] receivedValidAccessTokenFromURL:request.URL error:&error]) {
        if(!error) {
            [webView removeFromSuperview];
            if (callback) {
                callback(nil, [InstagramEngine sharedEngine].accessToken, nil);
            }
        }
    }
    return YES;
}

@end
