//
//  VMSocialUser.m
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 20/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSocialUser.h"

@implementation VMSocialUser

- (NSString *)fullname
{
    if (!_fullname) {
        return [NSString stringWithFormat:@"%@ %@", self.firstname, self.lastname];
    }
    return _fullname;
}

@end
