//
//  VMSocialManager.m
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 20/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSocialManager.h"

@implementation VMSocialManager

@synthesize authorizedUser = authorizedUser;

- (VMSocialUser *)authorizedUser
{
	return authorizedUser;
}

- (void)authorizeWithProvider:(id <VMSocialProvider> )provider andWithBlock:(VMSocialAuthBlock)block;
{
	activeProvider = provider;
	
	[provider authorizeWithBlock:^(VMSocialUser *user, NSString *accessToken, NSError *error) {
		authorizedUser = [user copy];
		if (block) {
			block(user, accessToken, error);
		}
	}];
}

- (BOOL)isAuthorized;
{
	[NSException raise:@"Missing implementation" format:@"You must override isAuthorized method"];
	return NO;
}

@end
