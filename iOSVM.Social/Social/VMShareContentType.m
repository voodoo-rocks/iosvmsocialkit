//
//  ShareContentType.m
//  iOSVM.Social
//
//  Created by Mihail Kirillov on 7/6/15.
//  Copyright (c) 2015 Voodoo Mobile. All rights reserved.
//

#import "VMShareContentType.h"

@implementation VMShareContentType

- (instancetype)init;
{
    self = [super init];
    if(self)
    {
        names = @[@"link",@"photo", @"video"];
    }
    return self;
}

@end
