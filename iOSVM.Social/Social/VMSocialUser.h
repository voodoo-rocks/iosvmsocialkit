//
//  VMSocialUser.h
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 20/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "IOSVMCoreKit/VMEntity.h"

@interface VMSocialUser : VMEntity

@property (copy) NSString *email;
@property (copy) NSString *firstname;
@property (copy) NSString *lastname;
@property (copy) NSString *uid;
@property (copy) NSString *photoLink;
@property (copy) NSString *location;
@property (copy) NSString *socialNetName;
@property (copy) NSString *username;

@property (nonatomic, copy) NSString *fullname;

@end
