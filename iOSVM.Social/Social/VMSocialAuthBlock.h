//
//  VMSocialAuthBlock.h
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 20/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMSocialUser.h"

typedef void(^VMSocialAuthBlock)(VMSocialUser *user, NSString *accessToken, NSError *error);