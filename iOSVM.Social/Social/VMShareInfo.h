//
//  VMShareInfo.h
//  Malco
//
//  Created by Alexander Kryshtalev on 23/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMShareContentType.h"

@interface VMShareInfo : VMEntity

@property (copy) NSString *description;
@property (copy) NSString *imageLink;
@property (copy) NSString *videoLink;
@property (copy) NSString *title DEPRECATED_MSG_ATTRIBUTE("`title` is deprecated (it does nothing in Facebook Graph API 2.9)");
@property (copy) NSString *link;
@property (copy) NSArray *images;
@property (copy) VMShareContentType *type;

@end
