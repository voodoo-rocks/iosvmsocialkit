//
//  VMTwitterSocialProvider.h
//  Malco
//
//  Created by Alexander Kryshtalev on 23/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "VMSocialProvider.h"

@interface VMTwitterSocialProvider : NSObject <VMSocialProvider>
{
    VMSocialAuthBlock authBlock;
    SLComposeViewController *twitterComposer;
}

- (void)authorizeWithBlock:(VMSocialAuthBlock)block;
- (void)share:(VMShareInfo *)info;
- (void)shareWithUi:(VMShareInfo *)info;
- (void)shareWithUi:(VMShareInfo *)info withBlock:(VMCompletionBlock)block;

@property (copy) NSString *userName;

@end
