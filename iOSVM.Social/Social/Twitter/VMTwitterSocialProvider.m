//
//  VMTwitterSocialProvider.m
//  Malco
//
//  Created by Alexander Kryshtalev on 23/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMTwitterSocialProvider.h"
#import <IOSVMCoreKit/VMFlashManager.h>
#import <IOSVMCoreKit/VMActionSheet.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "VMSocialUser.h"

typedef void (^ AccountBlock)(ACAccount *account, NSError *error);

@implementation VMTwitterSocialProvider

- (void)authorizeWithBlock:(VMSocialAuthBlock)block;
{
    authBlock = ^(VMSocialUser *user, NSString *accessToken, NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (block) {
                block (user, accessToken, error);
            }
        });
    };
    
    [self selectAccount:^(ACAccount *account, NSError *error) {
        if (account) {
            [self requestInfoForAccount:account];
        }
        else {
            authBlock (nil, nil, error);
        }
    }];
}

- (void)selectAccount:(AccountBlock)block
{
    ACAccountStore *twitterAccount = [[ACAccountStore alloc] init];
    ACAccountType *accountTypeTwitter = [twitterAccount accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [twitterAccount requestAccessToAccountsWithType:accountTypeTwitter options:nil completion:^(BOOL granted, NSError *error) {
        if(granted && [twitterAccount accountsWithAccountType:accountTypeTwitter].count > 0) {
            [self selectAccountFromArray:[twitterAccount accountsWithAccountType:accountTypeTwitter] withBlock:block];
        }
        else {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [[VMFlashManager sharedManager] showFlash:@"Please allow access to Twitter in settings"];
            }];
            if (block) {
                block (nil, error);
            }
        }
    }];
}

- (void)selectAccountFromArray:(NSArray *)accounts withBlock:(AccountBlock)block
{
    if (accounts.count == 1) {
        if (block) {
            block (accounts.lastObject, nil);
        }
        return;
    }
    
    if (self.userName) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username LIKE[cd] %@", self.userName];
        NSArray *found = [accounts filteredArrayUsingPredicate:predicate];
        if (found.count > 0) {
            if (block) {
                block (found.lastObject, nil);
            }
            return;
        }
    }
    
    VMActionSheet *actionSheet = [[VMActionSheet alloc] initWithTitle:@"Choose a Twitter account" cancelButton:nil destructiveButton:nil otherButtons:nil];
    for (ACAccount *account in accounts) {
        [actionSheet addButton:[VMActionButton buttonWithTitle:account.username withAction:^{
            if (block) {
                block (account, nil);
            }
        }]];
    }
    [actionSheet addButton:[VMActionButton buttonWithTitle:@"Cancel" withAction:^{
        if (block) {
            block (nil, nil);
        }
    }]];
    actionSheet.cancelButtonIndex = accounts.count;
    [actionSheet performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
}

- (void)requestInfoForAccount:(ACAccount *)twitterAccount
{
    NSDictionary *params = [NSDictionary dictionaryWithObject:twitterAccount.username
                                                       forKey:@"screen_name"];
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/users/show.json"];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:url parameters:params];
    request.account = twitterAccount;
    [request performRequestWithHandler:
     ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
         if (responseData) {
             NSDictionary *response =
             [NSJSONSerialization JSONObjectWithData:responseData
                                             options:NSJSONReadingAllowFragments
                                               error:NULL];
             NSLog(@"Twitter response %@", response);
             
             VMSocialUser *user = [[VMSocialUser alloc] init];
             
             user.uid = [response objectForKey:@"id"];
             user.fullname = [response objectForKey:@"name"];
             user.photoLink = [response objectForKey:@"profile_image_url"];
             user.socialNetName = self.socialNetworkName;
             user.username = twitterAccount.username;

             authBlock (user, nil, nil);

         }
         else {
             authBlock (nil, nil, error);
         }
     }];
}

- (void)share:(VMShareInfo *)info;
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])     {
        twitterComposer = [[SLComposeViewController alloc] init]; //initiate the Social Controller
        twitterComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter]; //Tell him with what social plattform to use it, e.g. facebook or twitter
        [twitterComposer setInitialText:info.description]; //the message you want to post
        [twitterComposer addImage:info.images.firstObject];
        [twitterComposer addURL:[NSURL URLWithString:info.link]];
        
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:twitterComposer animated:YES completion:nil];
        [twitterComposer setCompletionHandler:^(SLComposeViewControllerResult result) {
            if (result == SLComposeViewControllerResultDone) {
                [[VMFlashManager sharedManager] showFlash:@"Successfully published to Twitter"];
            }
        }];
    }
    else {
        [[VMFlashManager sharedManager] showFlash:@"Please allow access to Twitter in settings"];
    }
}

- (void)shareWithUi:(VMShareInfo *)info;
{
    [self shareWithUi:info withBlock:nil];
}

- (void)shareWithUi:(VMShareInfo *)info withBlock:(VMCompletionBlock)block;
{
    VMCompletionBlock completionBlock = ^(BOOL succeeded, NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (block) {
                block (succeeded, error);
            }
        });
    };
    [self selectAccount:^(ACAccount *account, NSError *error) {
        if (account) {
            NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
            NSDictionary *params = @{@"status" : info.description};
            SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                    requestMethod:SLRequestMethodPOST
                                                              URL:url
                                                       parameters:params];
            [request setAccount:account];
            [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                if (responseData) {
                    NSDictionary *response =
                    [NSJSONSerialization JSONObjectWithData:responseData
                                                    options:NSJSONReadingAllowFragments
                                                      error:NULL];
                    NSLog(@"Twitter response %@", response);
                    if ([response objectForKey:@"errors"]) {
                        NSDictionary *error = [[response objectForKey:@"errors"] firstObject];
						
						[[VMFlashManager sharedManager] showFlash:[NSString stringWithFormat:@"Twitter: %@", [error objectForKey:@"message"]]];
						
                        completionBlock (NO, nil);
                    }
                    else {
                        completionBlock (YES, nil);
                    }
                }
                else {
                    completionBlock (NO, error);
                }
            }];
        }
        else {
            completionBlock (NO, error);
        }
    }];
}

- (NSString *)socialNetworkName;
{
    return @"twitter";
}

- (void)friendsListWithBlock:(VMArrayBlock)block
{
    [self selectAccount:^(ACAccount *account, NSError *error) {
        if (account) {
            NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/friends/list.json"];
            NSDictionary *params = @{@"screen_name" : account.username,
                                     @"count" : [NSNumber numberWithInt:100]};
            SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                    requestMethod:SLRequestMethodGET
                                                              URL:url
                                                       parameters:params];
            [request setAccount:account];
            [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                if (responseData) {
                    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseData
                                                                             options:NSJSONReadingAllowFragments
                                                                               error:NULL];
                    NSLog(@"Twitter response %@", response);
                    if ([response objectForKey:@"errors"]) {
                        block(nil);
                    } else {
                        NSMutableArray *friends = [NSMutableArray array];
                        for (NSDictionary *twUser in [response objectForKey:@"users"]) {
                            VMSocialUser *user = [[VMSocialUser alloc] init];
                            user.uid = [twUser objectForKey:@"id"];
                            user.fullname = [twUser objectForKey:@"name"];
                            user.photoLink = [twUser objectForKey:@"profile_image_url"];
                            user.username = [twUser objectForKey:@"screen_name"];
                            user.socialNetName = self.socialNetworkName;
                            [friends addObject:user];
                        }
                        block(friends);
                    }
                } else {
                    block(nil);
                }
            }];
        } else {
            block(nil);
        }
    }];
}

@end
