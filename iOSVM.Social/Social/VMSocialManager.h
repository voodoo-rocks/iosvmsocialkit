//
//  VMSocialManager.h
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 20/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSocialUser.h"
#import "VMSocialProvider.h"
#import "VMSocialAuthBlock.h"
#import "IOSVMCoreKit/VMSharedManager.h"

@interface VMSocialManager : VMSharedManager
{
	VMSocialUser *authorizedUser;
	id <VMSocialProvider> activeProvider;
}

@property (nonatomic, readonly) VMSocialUser *authorizedUser;

- (void)authorizeWithProvider:(id <VMSocialProvider> )provider andWithBlock:(VMSocialAuthBlock)block;
- (BOOL)isAuthorized;

@end
