//
//  VMVkontakteSocialProvider.h
//  iOSVM.Social
//
//  Created by Mihail Kirillov on 10/5/15.
//  Copyright © 2015 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMSocialProvider.h"
#import "VMBlocks.h"

@interface VMVkontakteSocialProvider : NSObject<VMSocialProvider>
{
    VMSocialAuthBlock authBlock;
}

- (instancetype)initWithAppId:(NSString *)appId;

@end
