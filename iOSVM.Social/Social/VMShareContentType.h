//
//  ShareContentType.h
//  iOSVM.Social
//
//  Created by Mihail Kirillov on 7/6/15.
//  Copyright (c) 2015 Voodoo Mobile. All rights reserved.
//

#import "IOSVMCoreKit/VMNamedEnum.h"

typedef NS_ENUM(NSInteger, ContentType){
    ContentTypeLink,
    ContentTypePhoto,
    ContentTypeVideo
};

@interface VMShareContentType : VMNamedEnum

@end
