//
//  VMFacebookSocialProvider.m
//  Malco
//
//  Created by Alexander Kryshtalev on 23/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "IOSVMCoreKit/VMFlashManager.h"
#import "VMFacebookSocialProvider.h"
#import "VMSocialUser.h"
#import <Social/Social.h>
#import "VMActionSheet.h"

typedef void (^AccountBlock)(ACAccount *account, NSError *error);
typedef void(^PageBlock)(NSString *pageId, NSString *accessToken);

@implementation VMFacebookSocialProvider

- (void)authorizeWithBlock:(VMSocialAuthBlock)block;
{
    if ([FBSDKAccessToken currentAccessToken]) {
        [self getSocialUserWithCallback:block];
    } else {
        [[FBSDKLoginManager new] logInWithReadPermissions:@[@"email"] fromViewController:[UIApplication sharedApplication].keyWindow.rootViewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (!error) {
                [self getSocialUserWithCallback:block];
            }
        }];
    }
}

- (void)getSocialUserWithCallback:(VMSocialAuthBlock)block;
{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email, first_name, last_name"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         
         if (!error) {
             
             VMSocialUser *socialUser = [VMSocialUser new];
             
             socialUser.email = [result valueForKey:@"email"];
             socialUser.firstname = [result valueForKey:@"first_name"];
             socialUser.lastname = [result valueForKey:@"last_name"];
             socialUser.uid = [result valueForKey:@"id"];
             socialUser.photoLink = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [result objectForKey:@"id"]];
             socialUser.username = [result valueForKey:@"name"];
             socialUser.socialNetName = self.socialNetworkName;
             
             if (block) {
                 block(socialUser, [FBSDKAccessToken currentAccessToken].tokenString, error);
             }
         }
     }];
}

- (void)shareInfoChoosingPage:(VMShareInfo *)info
{
    NSArray *permissions = @[@"manage_pages", @"publish_actions", @"publish_pages"];
    
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_pages"]) {
        [self getPagesToShareContent:info];
    } else {
        [[FBSDKLoginManager new] logInWithPublishPermissions:permissions fromViewController:[UIApplication sharedApplication].keyWindow.rootViewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         {
             if (!error) {
                 [self getPagesToShareContent:info];
             }
         }];
    }
}

- (void)getPagesToShareContent:(VMShareInfo *)info
{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/accounts" parameters:@{@"fields": @""}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, NSDictionary *result, NSError *error)
     {
         if (!error)
         {
             [self choosePage:[result valueForKey:@"data"] withBlock:^(NSString *pageId, NSString *pageAccessToken)
              {
                  [[[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"%@/feed", pageId]
                                                     parameters:@{
                                                                  @"message" : info.description ? info.description : @"",
                                                                  @"link" : info.link ? info.link : @""
                                                                  }
                                                    tokenString:pageAccessToken version:@"v2.5" HTTPMethod:@"POST"]
                   startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                       if(!error) {
                           [[VMFlashManager sharedManager] showFlash:@"Shared successfully"];
                       }
                   }];
              }];
         }
     }];
}

- (void)choosePage:(NSArray *)pages withBlock:(PageBlock)block
{
    VMActionSheet *actionSheet = [[VMActionSheet alloc] initWithTitle:@"Choose where to share" cancelButton:[VMActionButton buttonWithTitle:@"Cancel" withAction:nil] destructiveButton:nil otherButtons:nil];
    
    [actionSheet addButton:[VMActionButton buttonWithTitle:@"My timeline" withAction:^{
        if(block) {
            block(@"me", [FBSDKAccessToken currentAccessToken].tokenString);
        }
    }]];
    
    for(NSDictionary *page in pages) {
        [actionSheet addButton:[VMActionButton buttonWithTitle:[page valueForKey:@"name"] withAction:^{
            if(block) {
                block([page valueForKey:@"id"], [page valueForKey:@"access_token"]);
            }
        }]];
    }
    
    [actionSheet performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
}

- (void)share:(VMShareInfo *)info;
{
    [FBSDKShareAPI shareWithContent:[self shareContentForInfo:info] delegate:self];
}

- (void)shareWithUi:(VMShareInfo *)info withBlock:(VMCompletionBlock)block;
{
    FBSDKShareDialog *dialog = [FBSDKShareDialog new];
    dialog.fromViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    dialog.shareContent = [self shareContentForInfo:info];
    dialog.mode = FBSDKShareDialogModeNative; // if you don't set this before canShow call, canShow would always return YES
    if (![dialog canShow]) {
        // fallback presentation when there is no FB app
        dialog.mode = FBSDKShareDialogModeFeedBrowser;
    }
    [dialog show];
}

- (id<FBSDKSharingContent>)shareContentForInfo:(VMShareInfo *)info
{
    switch (info.type.typeId) {
        case ContentTypeLink:
        {
            FBSDKShareLinkContent *linkContent = [FBSDKShareLinkContent new];
            linkContent.quote = info.description;
            linkContent.contentURL = [NSURL URLWithString:info.link];
            return linkContent;
        }
            break;
            
        case ContentTypePhoto:
        {
            NSMutableArray *photos = [NSMutableArray array];
            for (UIImage *image in info.images) {
                FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
                photo.image = image;
                photo.userGenerated = YES;
                [photos addObject:photo];
            }
            
            FBSDKSharePhotoContent *photoContent = [FBSDKSharePhotoContent new];
            photoContent.photos = photos;
            return photoContent;
        }
            break;
            
        case ContentTypeVideo:
        {
            NSURL *videoURL = [NSURL fileURLWithPath:info.videoLink];
            
            FBSDKShareVideo *video = [FBSDKShareVideo new];
            video.videoURL = videoURL;
            FBSDKShareVideoContent *videoContent = [FBSDKShareVideoContent new];
            videoContent.video = video;
            return videoContent;
        }
            break;
            
        default:
            break;
    }
    
    
    return nil;
}

- (void)friendsListWithBlock:(VMArrayBlock)block;
{
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"user_friends"]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (block) {
                 block([result valueForKey:@"data"]);
             }
         }];
    } else {
        [[FBSDKLoginManager new] logInWithReadPermissions:@[@"user_friends"] fromViewController:[UIApplication sharedApplication].keyWindow.rootViewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (!error) {
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends" parameters:nil]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                     if (block) {
                         block([result valueForKey:@"data"]);
                     }
                 }];
            }
        }];
    }
}

- (NSString *)socialNetworkName;
{
    return @"facebook";
}

#pragma mark Share delegate methods

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results;
{
    [[VMFlashManager sharedManager] showFlash:[NSString stringWithFormat:@"Sharing completed, results: %@", results]];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error;
{
    [[VMFlashManager sharedManager] showFlash:[NSString stringWithFormat:@"Sharing failed, error: %@", error]];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer;
{
    [[VMFlashManager sharedManager] showFlash:@"Sharing canceled"];
}

@end
