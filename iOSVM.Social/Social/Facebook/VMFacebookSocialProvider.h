//
//  VMFacebookSocialProvider.h
//  Malco
//
//  Created by Alexander Kryshtalev on 23/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>
#import "VMSocialProvider.h"
#import <Social/Social.h>

#import "FBSDKShareKit/FBSDKShareKit.h"
#import "FBSDKLoginKit/FBSDKLoginKit.h"
#import "FBSDKCoreKit/FBSDKCoreKit.h"

@interface VMFacebookSocialProvider : NSObject <VMSocialProvider, FBSDKSharingDelegate>

- (void)shareInfoChoosingPage:(VMShareInfo *)info;

@end
