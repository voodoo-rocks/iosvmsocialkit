//
//  VMVkontakteSocialProvider.m
//  iOSVM.Social
//
//  Created by Mihail Kirillov on 10/5/15.
//  Copyright © 2015 Voodoo Mobile. All rights reserved.
//

#import "VMVkontakteSocialProvider.h"
#import "VMPopupManager.h"
#import "VMPlistOptions.h"
#import "VKSdk.h"

@interface VMVkontakteSocialProvider() <VKSdkDelegate>
@end

@implementation VMVkontakteSocialProvider

- (instancetype)initWithAppId:(NSString *)appId
{
    self = [super init];
    if (self) {
        [[VKSdk initializeWithAppId:appId] registerDelegate:self];
    }
    return self;
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    NSLog(@"%@", result);
}

- (void)vkSdkUserAuthorizationFailed
{
    NSLog(@"Auth failed");
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    NSLog(@"vkSdkNeedCaptchaEnter");
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken
{
    NSLog(@"vkSdkReceivedNewToken, token: %@", newToken);
    
    [[[VKApi users] get] executeWithResultBlock:^(VKResponse *response) {
        NSLog(@"%@", response);
    } errorBlock:^(NSError *error) {
        NSLog(@"%@", error.localizedDescription);
    }];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    NSLog(@"vkSdkTokenHasExpired");
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    NSLog(@"vkSdkShouldPresentViewController");
    
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError
{
    NSLog(@"vkSdkUserDeniedAccess");
}

- (void)vkSdkWillDismissViewController:(UIViewController *)controller;
{
    NSLog(@"vkSdkWillDismissViewController");
}

- (void)vkSdkDidDismissViewController:(UIViewController *)controller;
{
    NSLog(@"vkSdkDidDismissViewController");
}

- (void)authorizeWithBlock:(VMSocialAuthBlock)block;
{
    authBlock = block;
    [VKSdk authorize:@[VK_PER_EMAIL,VK_PER_OFFLINE, VK_PER_NOHTTPS]];
}

- (void)share:(VMShareInfo *)info;
{
    
}

- (void)shareWithUi:(VMShareInfo *)info withBlock:(VMCompletionBlock)block;
{
    
}

- (NSString *)socialNetworkName;
{
    return @"Vkontakte";
}

@end
