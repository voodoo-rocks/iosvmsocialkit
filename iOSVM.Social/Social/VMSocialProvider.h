//
//  VMSocialProvider.h
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 20/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IOSVMCoreKit/VMEntity.h"
#import "IOSVMCoreKit/VMBlocks.h"
#import "VMSocialAuthBlock.h"
#import "VMShareInfo.h"

@protocol VMSocialProvider <NSObject>
@required

- (void)authorizeWithBlock:(VMSocialAuthBlock)block;
- (void)share:(VMShareInfo *)info;
- (void)shareWithUi:(VMShareInfo *)info withBlock:(VMCompletionBlock)block;

- (NSString *)socialNetworkName;

@optional

- (void)friendsListWithBlock:(VMArrayBlock)block;

@end