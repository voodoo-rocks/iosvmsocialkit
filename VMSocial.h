//
//  VMSocial.h
//  iOSVM.Social
//
//  Created by Alexander Kryshtalev on 02/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VMSocialUser.h"
#import "VMSocialManager.h"
#import "VMShareInfo.h"

@interface VMSocial : NSObject

+ (NSString *)libraryVersion;

@end
