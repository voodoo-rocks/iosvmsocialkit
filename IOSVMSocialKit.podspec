Pod::Spec.new do |s|
  s.name             = "IOSVMSocialKit"
  s.version          = "1.1.0"
  s.summary          = "Useful set of timesaving tools"
  s.homepage         = "https://voodoo-rocks@bitbucket.org/voodoo-rocks/iosvmsocialkit.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Voodoo Mobile" => 'public@voodoo-rocks.com' }
  s.source           = { :git => 'https://voodoo-rocks@bitbucket.org/voodoo-rocks/iosvmsocialkit.git', branch:'master', :tag => s.version.to_s}

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'iOSVM.Social/**/*.{h,m}'

    s.public_header_files = 'iOSVM.Social/**/*.h'
    s.frameworks = 'Social', 'Accounts', 'UIKit'
    s.dependency 'IOSVMCoreKit'
    s.dependency 'FBSDKCoreKit'
    s.dependency 'FBSDKLoginKit'
    s.dependency 'FBSDKShareKit'
    s.dependency 'VK-ios-sdk'
    s.dependency 'InstagramKit'

end
