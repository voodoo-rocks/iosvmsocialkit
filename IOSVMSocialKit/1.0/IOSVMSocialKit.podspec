Pod::Spec.new do |s|
  s.name             = "IOSVMSocialKit"
  s.version          = "1.0"
  s.summary          = "Useful set of timesaving tools"
  s.homepage         = "https://voodoo-mobile@bitbucket.org/voodoo-mobile/iosvmsocialkit.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Voodoo Mobile" => 'public@voodoo-mobile.com' }
  s.source           = { :git => 'https://voodoo-mobile@bitbucket.org/voodoo-mobile/iosvmsocialkit.git', branch:'master', tag:'1.0'}

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.xcconfig = { 'CLANG_ENABLE_MODULES' => 'NO' }

  s.source_files = 'iOSVM.Social/**/*.{h,m}'
  s.resource_bundles = {
    'iOSVM.Social' => ['Pod/Assets/*.png']
  }

    s.public_header_files = 'iOSVM.Social/**/*.h'
    s.frameworks = 'Social', 'Accounts', 'UIKit'
    s.dependency 'IOSVMCoreKit', '1.0'
    s.dependency 'FBSDKCoreKit', '~> 4.3'
    s.dependency 'FBSDKLoginKit', '~> 4.3'
    s.dependency 'FBSDKShareKit', '~> 4.3'
    #s.dependency 'InstagramKit'
end
