//
//  VMSocial.m
//  iOSVM.Social
//
//  Created by Alexander Kryshtalev on 02/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSocial.h"

@implementation VMSocial

+ (NSString *)libraryVersion;
{
	return @"6.0 beta";
}

@end
